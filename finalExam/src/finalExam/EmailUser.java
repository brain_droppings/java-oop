package finalExam;

public class EmailUser extends User {

	String emailAddress;

	public EmailUser(String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		setEmailAddress(emailAddress);
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		if (emailAddress.contains("@") && emailAddress.contains(".")) {
			this.emailAddress = emailAddress;
		} else {
			System.out.println("Invalid input");
		}
		
	}
	
	
}
