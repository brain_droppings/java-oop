package finalExam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SendSms implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		return false;
	}

	@Override
	public void sendMessage(Message message) {
		
		File myObj = new File("src/finalExam/smsInfo.txt");
		
		try {
            if (myObj.createNewFile()) {
              System.out.println("File created: " + myObj.getName());
            } else {
              System.out.println("File already exists.");
            }
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }


        try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath()))) {
                writer.write(message.toString() + "\n");

        } catch (IOException e) {
            System.out.println("An error occured.");
            e.printStackTrace();
        }
	}

}
