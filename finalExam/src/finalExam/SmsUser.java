package finalExam;

public class SmsUser extends User{
	
	String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		setPhoneNumber(phoneNumber);
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber (String phoneNumber) {
		try {
            if (phoneNumber.matches("[0-9]+")) {
                this.phoneNumber = phoneNumber;
        }
      }
        catch(IllegalArgumentException e) {
            System.out.println("Invalid input.");
        }
		
		if (phoneNumber.length() != 10) {
			throw new IllegalArgumentException("Invalid Phone Number.");
		}
	}

}
