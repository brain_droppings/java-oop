package finalExam;

import java.util.ArrayList;

public class App {

	public static void main(String[] args) {
		
		ArrayList<Message> listOfMessages = new ArrayList<Message>();
		
		EmailMessage email = new EmailMessage(
				new EmailUser("Judy", "Foster", new Address("Main Street", 1), "a.d@g.com"),
				new EmailUser("Betty", "Beans", new Address("second street", 2), "v.r@g.com"),
				"Dear Judy, take this email and eat it for breakfast.");
		
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Judy", "Foster", new Address("Main Street", 1), "1221237891"), //changed the phone number to run the code
				new SmsUser("Betty", "Beans", new Address("second street", 2), "1232313891"), //changed the phone number to run the code
				"Sup giiirl, Let's go to that place and do that thing.");
		
		listOfMessages.add(email);
		listOfMessages.add(smsMessage);
		
		sendEmail sendEmail = new sendEmail();
        	sendEmail.sendMessage(email);
//        	sendEmail.validateMessage(sender, receiver, body);
        	
        SendSms sendSms = new SendSms();
        	sendSms.sendMessage(smsMessage);
        
        System.out.println(listOfMessages);
	}

}
