package finalExam;

public class Message {

	private User reciever;
	private User sender;
	private String body;
	
	public Message(User reciever, User sender, String body) {
		setReciever(reciever);
		setSender(sender);
		setBody(body);
	}

	public User getReciever() {
		return reciever;
	}

	public void setReciever(User reciever) {
		this.reciever = reciever;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {

		if (body.isEmpty() || body.contains("^") || body.contains("*") || body.contains("!")) {
			throw new IllegalArgumentException("Invail Message");
		} else {
			this.body = body;
		}
	}

	@Override
	public String toString() {
		return "Message [to:" + reciever + "\n" + "From:" + sender + "\n" + "Message:" + body + "]";
	}
	
	
}
