package wallArea;

import java.util.Scanner;

class Wall {
	double width;
	double height;
	
	static Scanner input = new Scanner(System.in);
	
	public Wall() {
		
	}
	
	public Wall(double w, double h) {
		this.width = w;
		this.height = h;
		if (w < 0) {
			w = 0;
		} else if (h < 0) {
			h = 0;
		}
	}
	
	double getWidth() {
		System.out.print("Enter the width: ");
		width = input.nextDouble();
		if (width < 0) {
			width = 0;
		}
		return width;
	}
	
	double getHeight() {
		System.out.print("Enter height: ");
		height = input.nextDouble();
		if (height < 0) {
			height = 0;
		}
		return height;
	}
	
	void setWidth(double wide) {
		
		if (width < 0) {
			width = 0;
		}
		width = wide;
	}
	
	void setHeight(double high) {
		
		if (height < 0) {
			height = 0;
		}
		height = high;
	}
	
	double getArea() {
		double area = height * width;
		return area;
	}
}

public class task3 {

	public static void main(String[] args) {
		Wall wall =  new Wall(5,4);
		System.out.println("area= " + wall.getArea());
		wall.setWidth(5.0);
		wall.setHeight(-1.5);
		System.out.println("width= " + wall.getWidth());
		System.out.println("height= " + wall.getHeight());
		System.out.println("area= " + wall.getArea());
	}
}
