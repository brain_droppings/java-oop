package person;


import java.util.Scanner;

class Person {
	String firstName;
	String lastName;
	int age;
	
	static Scanner input = new Scanner(System.in);
	
	void getFirstName() {
		System.out.print("What is your first name?");
		firstName = input.next();
	}
	
	void getLastName() {
		System.out.print("What is your last name?");
		lastName = input.next();
	}
	
	void getAge() {
		System.out.print("How old are you?");
		age = input.nextInt();
	}
	
	void setFirstName(String first) {
		firstName = first;
	}
	
	void setLastName(String last) {
		lastName = last;
	}
	
	int setAge(int setAge) {
		age = setAge;
		if (age < 0 || age > 100) {
			age = 0;
		}return age;
	}
	
	boolean isTeen() {
		if (age > 12 && age < 20) {
			return true;
		} else {
			return false;
		}
		
	}
	
	String getFullName() {
		
		if (firstName.isEmpty() && lastName.isEmpty()) {
			return "";
		} else if (lastName.isEmpty()) {
			return firstName;
		} else if (firstName.isEmpty()) {
			return lastName;
		} else {
			return firstName + " " + lastName;
		}
	}
}

public class task1 {

	public static void main(String[] args) {
		Person person = new Person();
		person.getFirstName();
		person.getLastName();
		person.getAge();
		System.out.println("fullName = " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		System.out.println("---------------------------------------");
		
		person.setFirstName("");
		person.setLastName("");
		person.setAge(10);
		System.out.println("fullName = " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setFirstName("John");
		person.setAge(18);
		System.out.println("fullName= " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setLastName("Smith");
		System.out.println("fullName= " + person.getFullName());
	}

}
