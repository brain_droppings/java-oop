package sumCalculator;


import java.util.Scanner;

class SimpleCalculator {
	double firstNumber;
	double secondNumber;
	
	static Scanner input = new Scanner(System.in);
	
	void getFirstNumber() {
		System.out.print("Enter first number: ");
		firstNumber = input.nextDouble();
	}
	
	void getSecondNumber() {
		System.out.print("Enter second number: ");
		secondNumber = input.nextDouble();
	}
	
	double setFirstNumber(double first) {
		firstNumber = first;
		return firstNumber;
	}
	
	double setSecondNumber(double second) {
		secondNumber = second;
		return secondNumber;
	}
	
	double getAdditionResult() {
		double sum = firstNumber + secondNumber;
		return sum;
	}
	
	double getSubstractionResult() {
		double difference = firstNumber - secondNumber;
		return difference;
	}
	
	double getMultiplicationResult() {
		double product = firstNumber * secondNumber;
		return product;
	}
	
	double getDivisionResult() {
		double quotient = firstNumber / secondNumber;
		if (secondNumber == 0) {
			quotient = 0;
		} 
		return quotient;
	}
}

public class task2 {

	public static void main(String[] args) {
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.getFirstNumber();
		calculator.getSecondNumber();
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("subtract= " + calculator.getSubstractionResult());
		System.out.println("---------------------------------------");
		
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("subtract= " + calculator.getSubstractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply= " + calculator.getMultiplicationResult());
		System.out.println("divide= " + calculator.getDivisionResult());
		
	}
}

