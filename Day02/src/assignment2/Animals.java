package assignment2;

public abstract class Animals {

	public abstract void Sound(); 
	
}

class Cats extends Animals {

@Override
public void Sound() {
	System.out.println("Meow");
	
	}


}

class Dogs extends Animals {

	@Override
	public void Sound() {
		System.out.println("Woof");
		
	}
	
	
}
