package assignment2;

public abstract class Shape {
	
	abstract double getArea(double base, double height);
	
	abstract double getPerimeter(double base, double height);
	
	
}

class Square extends Shape {
	
	@Override
	double getArea(double base, double height) {
		double area = base * height;
		return area;
	}

	@Override
	double getPerimeter(double base, double height) {
		double perimeter = (base * 2) + (height * 2);
		return perimeter;
	}


}

class Triangle extends Shape {

	@Override
	double getArea(double base, double height) {
		
		return 0;
	}

	@Override
	double getPerimeter(double base, double height) {

		return 0;
	}




	
}
