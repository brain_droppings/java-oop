package assign5;

public class ElectricBoogieDancer extends Dancer {
	
	public void dance(String name, int age) {
		System.out.println("Second dancer is " + name + " at the young age of " + age + ", using Electric moves that look like a hanging boogie in the wind!");
	}

}
