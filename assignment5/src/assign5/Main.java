package assign5;

import java.util.ArrayList;

import assign5.Breakdancer;
import assign5.Dancer;
import assign5.ElectricBoogieDancer;

public class Main {

	public static void main(String[] args) {
		
		Dancer dancer1 = new Dancer();
		Dancer dancer2 =  new ElectricBoogieDancer();
		Dancer dancer3 =  new Breakdancer();

		ArrayList<Dancer> notPopcorn = new ArrayList<Dancer>();
		
		notPopcorn.add(dancer1);
		notPopcorn.add(dancer2);
		notPopcorn.add(dancer3);
		
		dancer1.dance("Time-Travel Esetelle", 84);
		dancer2.dance("Shockingly Cool Mozard", 91);
		dancer3.dance("Helicopter Salvador", 87);
		
		for (int i = 0; i < notPopcorn.size(); i++) {
			notPopcorn.get(i).dance();
		}	
	}
}
