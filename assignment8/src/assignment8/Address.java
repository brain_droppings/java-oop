package assignment8;

public class Address {
	
	String buildingNo;
	String StreetName;
	String cityName;
	String provinceName;
	String postalCode;
	
	public Address(String buildingNo, String streetName, String cityName, String provinceName, String postalCode) {
		setBuildingNo(buildingNo);
		setStreetName(streetName);
		setCityName(cityName);
		setProvinceName(provinceName);
		setPostalCode(postalCode);
	}

	public String getBuildingNo() {
		return buildingNo;
	}

	public void setBuildingNo(String buildingNo) {
		this.buildingNo = buildingNo;
	}

	public String getStreetName() {
		return StreetName;
	}

	public void setStreetName(String streetName) {
		this.StreetName = streetName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
}
	

