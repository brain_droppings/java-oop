package studyTime;

public class TheOvertalkativeOne extends Client {

	public TheOvertalkativeOne(String name, String hatColor, double price, String flavour) {
		super(name, hatColor, price, flavour);
	}
	
	public void talkForHours() {
		System.out.println("Lorem Ipsum 800 words.");
	}

	@Override
	public void chooseType() {
		FlavoursAndPrices iceCream1 = new FlavoursAndPrices(2.99, "Vanilla");
		System.out.println("I can't decide... You choose for me.");
	}

	@Override
	public void chooseAmount() {
		System.out.println("I guess just... no wait actually I'll take... no no, nevermind that would be greedy. How many would you take? Ok Ok, I'll have 2 then.");
	}

	@Override
	public void payBill() {
		System.out.println("I actually missed 3 hours of work last week so my paycheck last Thursday was 20 dollars less than usual. Crazy, ehhhh!");
	}

}
