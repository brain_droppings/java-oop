package studyTime;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ColdAsIce {

	public static void main(String[] args) throws IOException {
		
		ArrayList<Client> clients = new ArrayList<Client>();
		
		Client client1 = new TheNiceOne("Jack", "Blue hat", 4.05, "Reeses Pieces");
		TheNiceOne nice1 = (TheNiceOne)client1;
		clients.add(nice1);
		
		clients.add(new TheNiceOne("Molly", "Yellow hat", 3.49, "Chocolate"));
		clients.add(new TheConspiracyTheorist("Benoit", "Aluminum cap", 3.99, "Melon"));
		clients.add(new TheRudeOne("Karen", "Curly wig", 3.99, "Caramel Swirl"));
		clients.add(new TheOvertalkativeOne("Trinity", "Fur hoody", 2.99, "Vanilla"));
		
		File myObj = new File("src/studyTime/Receipt/receipt.txt");
		
		try {
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		  try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath()))) {
			  for(Client a : clients) {
				  writer.write(a.toString() + "\n");
			  } 

		  } catch (IOException e) {
			  System.out.println("An error occured.");
			  e.printStackTrace();
		  }
		  
		  try(BufferedReader reader = new  BufferedReader(new FileReader("src/studyTime/Receipt/receipt.txt"))){
	            Stream<String> lines = reader.lines();
	            lines.forEach(line -> System.out.println(line));
	        } catch (IOException e) {
	            throw e;
	        }

	}

}


