package studyTime;

import java.util.HashMap;

public class FlavoursAndPrices {

	private double price;
	private String flavor;
	
	public FlavoursAndPrices(double price, String flavor) {
		setPrice(price);
		setFlavor(flavor);
	}
	
//	FlavoursAndPrices iceCream1 = new FlavoursAndPrices(2.99, "Vanilla");
//	FlavoursAndPrices iceCream2 = new FlavoursAndPrices(3.49, "Chocolate");
//	FlavoursAndPrices iceCream3 = new FlavoursAndPrices(3.99, "Melon");
//	FlavoursAndPrices iceCream4 = new FlavoursAndPrices(3.99, "Caramel Swirl");
//	FlavoursAndPrices iceCream5 = new FlavoursAndPrices(4.05, "Reeses Pieces");
	
//	HashMap<Double, String> flavourMap = new HashMap<Double, String>(); {
//		flavourMap.put(iceCream1.getPrice(), iceCream1.getFlavor());
//		flavourMap.put(iceCream2.getPrice(), iceCream2.getFlavor());
//		flavourMap.put(iceCream3.getPrice(), iceCream3.getFlavor());
//		flavourMap.put(iceCream4.getPrice(), iceCream4.getFlavor());
//		flavourMap.put(iceCream5.getPrice(), iceCream5.getFlavor());
//	}
	
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getFlavor() {
		return flavor;
	}
	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	
	
}
