package studyTime;

public class TheNiceOne extends Client {
	
	
	
	public TheNiceOne(String name, String hatColor, double price, String flavour) {
		super(name, hatColor, price, flavour);
	}

	
	
	public void greet() {
		System.out.println("Oh heya! How you doing?");
	}

	@Override
	public void chooseType() {
		
//		FlavoursAndPrices iceCream2 = new FlavoursAndPrices(3.49, "Chocolate");
		System.out.println();
	}

	@Override
	public void chooseAmount() {
		System.out.println("This many scoops, please!");
	}

	@Override
	public void payBill() {
		System.out.println("Here you go!");
	}
	
	public void tip() {
		System.out.println("Keep the change!");
	}


}
