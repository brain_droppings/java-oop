package studyTime;

import java.util.ArrayList;

public interface EmployeeTasks {

	void takeOrder();
	void scoopIceCream();
	void calculatePrice();
	void useRegister();
	void giveReceipt();
	void calculatePrice(ArrayList<Client> clients);
}
