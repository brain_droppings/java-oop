package studyTime;

public class TheConspiracyTheorist extends Client {

	public TheConspiracyTheorist(String name, String hatColor, double price, String flavour) {
		super(name, hatColor, price, flavour);
	}
	
	public void beCovidiot() {
		System.out.println("Why do I need to wear a mask? It's all fake anyways...");
	}

	@Override
	public void chooseType() {
		FlavoursAndPrices iceCream3 = new FlavoursAndPrices(3.99, "Melon");
		System.out.println("Did you knooooow, that vanilla was thought of by an MK Ultra guinea pig? I can't support that... I'll go with.. Melon.");
	}

	@Override
	public void chooseAmount() {
		System.out.println("One scoop... which is NOT the shape of the Earth, by the waaaay.");
	}

	@Override
	public void payBill() {
		System.out.println("Million of people touched this 20 dollar bill. You sure you still want it?");
	}
	
	public void talkWayOutOfTipping() {
		System.out.println("Tipping encourages big corporations to pay minimum wage. I'm doing you a favor by not tipping you right now.");
	}
}
